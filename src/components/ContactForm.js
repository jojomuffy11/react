import "./ContactFormStyles.css";

function ContactForm(){
    return(
        <div className="form-container">
            <h1>Let's Catch Up</h1>
            <form action="https://formspree.io/f/mkndrnpd" method="POST">
                <input type="text" name="Name"  placeholder="Name" autoComplete="off" required/>
                <input  type="email" name="Email" placeholder="Email" autoComplete="off" required/>
                <input type="text" name="Subject" placeholder="Subject" autoComplete="off" required/>
                <textarea name="Message" placeholder="Message" rows="4" autoComplete="off" required></textarea>
                <button>Send Message</button>
            </form>

        </div>
        
    )
}

export default ContactForm