import Pyramid from "../assets/2-giza-getty.jpg";
import Ballon from "../assets/turkey-dest.jpg";
import TowerParis from "../assets/eiffel-tower-in-paris-151-medium.jpg";
import Pool from "../assets/fairypools.jpg";

import DestinationData from "./DestinationData";
import "./DestinationStyles.css"


const Destination =() =>{
    return(
<div className="destination">
    <h1>Popular Destinations</h1>
    <p>Not all those who wander are lost.</p>

    {/* <div className="first-des">
        <div className="des-text">
            <h2>Great Pyramid of Giza,Egypt</h2>
            <p>The last remaining of the Seven Wonders of the ancient world, the great pyramids of Giza, are perhaps the most famous and discussed structures in history.These massive monuments were unsurpassed in height for thousands of years after their construction and continue to amaze and enthrall us with their overwhelming mass and seemingly impossible perfection. </p>


        </div>
        <div className="image">
            <img alt="img" src={Pyramid} />
            <img alt="img" src={Ballon} />
        </div>
    </div> */}
<DestinationData
className="first-des"
heading="Great Pyramid of Giza,Egypt"
text="The last remaining of the Seven Wonders of the ancient world, the great pyramids of Giza, are perhaps the most famous and discussed structures in history.These massive monuments were unsurpassed in height for thousands of years after their construction and continue to amaze and enthrall us with their overwhelming mass and seemingly impossible perfection."
img1={Pyramid}
img2={Ballon}

/>

<DestinationData
className="first-des-reverse"
heading="Eiffel Tower, Paris"
text="The proposed tower had been a subject of controversy, drawing criticism from those who did not believe it was feasible and those who objected on artistic grounds. Prior to the Eiffel Tower's construction, no structure had ever been constructed to a height of 300 m, or even 200 m for that matter,"

img1={TowerParis}
img2={Pool}

/>
</div>
    )
}
export default Destination;