import "./FooterStyles.css";


const Footer = () =>{
    return(
        <div className="footer">
            <div className="top">
<div>
    <h1>Tourice</h1>
    <p>Go crazy by travelling</p>
</div>
<div>
    <a href="https://www.facebook.com/campaign/landing.php?campaign_id=14884913640&extra_1=s%7Cc%7C550525804791%7Cb%7Cfacebook%7C&placement=&creative=550525804791&keyword=facebook&partner_id=googlesem&extra_2=campaignid%3D14884913640%26adgroupid%3D128696220912%26matchtype%3Db%26network%3Dg%26source%3Dnotmobile%26search_or_content%3Ds%26device%3Dc%26devicemodel%3D%26adposition%3D%26target%3D%26targetid%3Dkwd-592856129%26loc_physical_ms%3D9062140%26loc_interest_ms%3D%26feeditemid%3D%26param1%3D%26param2%3D&gad_source=1&gclid=CjwKCAiAivGuBhBEEiwAWiFmYX2L_jO4M4TWo39gUhsEJ2dsjFhZACYT_bEad15xnN4E5e_FWUqCPRoCtNgQAvD_BwE">
        <i className="fa-brands fa-facebook-square"></i>
    </a>
    <a href="/">
        <i className="fa-brands fa-twitter-square"></i>
    </a>
    <a href="/">
        <i className="fa-brands fa-instagram-square"></i>
    </a>
    </div>
</div>


            <div className="bottom">
                <div>
                    <h4>Project</h4>
                    <a href="/">Catalog</a>
                    <a href="/">Status</a>
                    <a href="/">License</a>
                    <a href="https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox">swati.shah.fit.ece19@teamfuture.in</a>
                </div>

                <div>
                    <h4>Community</h4>
                    <a href="/">Issues</a>
                    <a href="/">Plan</a>
                    <a href="/">Explore</a>

                </div>

                <div>
                    <h4>Help</h4>
                    <a href="/">Aids</a>
                    <a href="/">Booking</a>
                    <a href="/">Travel</a>

                </div>
            </div>

        </div>
    )
}

export default Footer;