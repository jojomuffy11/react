import { Component } from "react";
import "./NavigationbarrStyles.css";
import {MenuItems} from "./MenuItems";
import {Link} from "react-router-dom";
class Navigationbarr extends Component{
     
    state={clicked: false}
    handleClick=()=>{
        this.setState({ clicked: !this.state.clicked})
    }
    render(){
        return(
            <nav className="NavbarItems">
                <h1 className="Navbar-logo">Tourice</h1>
                <div className="menu-icons" onClick={this.handleClick}>
                    <i className={this.state.clicked ? "fas fa-times" : "fas fa-bars"}></i>
                    
                </div>
                <ul className={this.state.clicked ? "nav-menu active" : "nav-menu"}>
                    {MenuItems.map((item, index)=>{
                        return(
                            <li key={index}>
                        <Link className={item.cName} to={item.url}>
                        <i className={item.icon}></i>{item.title}</Link>
                    </li>
                        )
                    })}

                    <button style={{padding:"0.5rem 1rem",whiteSpace:"nowrap",borderRadius:".3rem",fontSize:"1rem",fontWeight:"600",border:"none",cursor:"pointer",transition:".2s ease-in-out"}} ><Link to={'/Register'} style={{textDecoration:"none"}}>Register</Link></button>
                    <button style={{padding:"0.5rem 1rem",whiteSpace:"nowrap",borderRadius:".3rem",fontSize:"1rem",fontWeight:"600",border:"none",cursor:"pointer",transition:".2s ease-in-out"}}project ><Link to={'/Login'} style={{textDecoration:"none"}}>Login</Link></button>
                    <button style={{padding:"0.5rem 1rem",whiteSpace:"nowrap",borderRadius:".3rem",fontSize:"1rem",fontWeight:"600",border:"none",cursor:"pointer",transition:".2s ease-in-out"}}project ><Link to={'/Login'} style={{textDecoration:"none"}}>logout</Link></button>

                </ul>

            </nav>
        )
    }
}

export default Navigationbarr