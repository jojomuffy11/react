import "./AboutUsStyles.css"


function AboutUs (){
    return (
        <div className="about-container">
            <h1>Why Tourice?</h1>
            <p>Internet access and some travel smarts are all you need to get the best available deals on hotel bookings. So give your trusted offline travel agent a break, and discover the multiple perks (and tremendous savings) that come with DIY online bookings. Here are five reasons why it makes more sense to make your hotel bookings </p>


            <h1>Competitive Pricing and Special Logged-in Pricing</h1>
            <p>Since online travel agencies make bookings in bulk, they are able to offer you rates that you probably won’t find anywhere else. They have tie-ups with hotel chains that allow them to offer special package deals and add-ons for customers. But that’s not it. Allow us to let you in on a little secret—did you know that you are likely to be offered a special price if you are logged in to a travel app? </p>


            <h1>Pay at Checkout</h1>
            <p>Unlike direct bookings, where you have to pay 100% of the hotel tariff upfront just to book your room, booking through a travel app like MakeMyTrip comes with an element of trust.</p>

        </div>
    )
}

export default AboutUs