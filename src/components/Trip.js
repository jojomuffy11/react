import "./TripStyles.css"
import TripData from "./TripData";
import Trip1 from "../assets/Santorini-Greece.jpg";
import Trip2 from "../assets/Temple-in-India_600.jpg";
import Trip3 from "../assets/paracycling.jpg";


function Trip(){
    return(
        <div className="trip">
            <h1>Recent Trips</h1>
            <p>Explore places like you explore food.</p>
            <div className="tripcard">
                <TripData
                image={Trip1}
                heading="Trip to Greece"
                text="Begin your special journey together by visiting the beautiful Santorini, one can try the world-class Greek wines here. Visit Athens to catch up on the History of mankind, and don’t miss the Parthenon"
                
                />

<TripData
                image={Trip2}
                heading="Trip to India"
                text="Temples in India are one of the most valuable assets. Mystical 1000 years old temples in India are a few of the factors for which India is famous for. Kaliasanatha temple built in the 8th century is said to be the oldest temple after Mundeshwari Devi temple in Bihar."
                
                />

<TripData
                image={Trip3}
                heading="Trip to Malaysia"
                text="Santorini, known since ancient times as Thira, is one of the most famous islands in the world. The fact that you can sit in front of the caldera, enjoy local dishes, a drink"
                
                />
            </div>

        </div>
    )
}

export default Trip