
import './App.css';
import {Route,Routes} from "react-router-dom";
// import Navigationbarr from './components/Navigationbarr';
import Home from "./pages/Home";
import About from "./pages/About";
import Service from "./pages/Service";
import Contact from "./pages/Contact";
import Register from "./components/Register";
import LoginPage from './components/LoginPage';



function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/service" element={<Service />} />
        <Route path="/contact" element={<Contact />} />
        <Route  path="/Register" element={<Register />}/>
        <Route  path="/Login" element={<LoginPage />}/>
      </Routes>
     
      {/* <Navigationbarr /> */}
      {/* <Home /> */}
    </div>
  );
}

export default App;
