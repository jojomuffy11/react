import Hero from "../components/Hero";
import Navigationbarr from "../components/Navigationbarr";
import AboutImg from "../assets/adventure-tourism_1200.jpg";
import Footer from "../components/Footer";
import ContactForm from "../components/ContactForm";
function Service () {
    return(
        <div>
            <Navigationbarr />
            <Hero
            cName="hero-mid"
            heroImg={AboutImg}
            title="Contact"
            // text="Tan lines and jetlag fade, but memories last forever."
            
            />
            <ContactForm />
<Footer />
        </div>
    )
}

export default Service;