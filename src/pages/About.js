import Hero from "../components/Hero";
import Navigationbarr from "../components/Navigationbarr";
import AboutImg from "../assets/travelbackground image.jpg";
import Footer from "../components/Footer";
import AboutUs from "../components/AboutUs";
function About () {
    return(
        <div>
            <Navigationbarr />
            <Hero
            cName="hero-mid"
            heroImg={AboutImg}
            title="About"
            // text="Tan lines and jetlag fade, but memories last forever."
            
            />
            <AboutUs />
<Footer />
        </div>
    )
}

export default About;