import Hero from "../components/Hero";
import Navigationbarr from "../components/Navigationbarr";
import AboutImg from "../assets/services-tour.jpg";
import Footer from "../components/Footer";
import Trip from "../components/Trip";
function Service () {
    return(
        <div>
            <Navigationbarr />
            <Hero
            cName="hero-mid"
            heroImg={AboutImg}
            title="Service"
            // text="Tan lines and jetlag fade, but memories last forever."
            
            />
            <Trip />
            <Footer />
        </div>
    )
}

export default Service;