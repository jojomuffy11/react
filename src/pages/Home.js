import Destination from "../components/Destination";
import Hero from "../components/Hero";
import Trip from "../components/Trip"
import Navigationbarr from "../components/Navigationbarr";
import Footer from "../components/Footer";
function Home () {
    return(
        <div>
            <Navigationbarr />
            {/* <h1>Home</h1> */}
            <Hero
            cName="hero"
            heroImg="https://images.unsplash.com/photo-1684278960143-7fea0a4a7212?q=80&w=1770&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            title="Keep calm and travel on."
            text="Tan lines and jetlag fade, but memories last forever."
            
            />
            <Destination />
            <Trip />
            <Footer />
        </div>
    )
}

export default Home;